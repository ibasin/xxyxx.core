﻿using Foundation;
using Supermodel.Mobile.Runtime.iOS.App;
using XXYXX.Mobile.AppCore;

namespace XXYXX.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : iOSFormsApplication<XXYXXApp> { }
}